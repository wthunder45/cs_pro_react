import React from 'react';
import {
  Routes,
  Route
} from 'react-router-dom';

import CustomNavbar from './pages/generalcomponents/CustomNavbar';
import HomePage from './pages/homecomponents/HomePage';
import AboutPage from './pages/aboutcomponents/AboutPage';
import UserLogin from './pages/userpages/UserLogin';
import UserSignup from './pages/userpages/UserSignup';
import GeneralTeamPage from './pages/teamcomponents/GeneralTeamPage';
import SignupSuccessPage from './pages/userpages/SignupSuccessPage';
import UserCookieTest from './pages/userpages/UserCookieTest';
import CreateTeamPage from './pages/teamcomponents/CreateTeamPage';

function App() {
  return (
    <div className="App bg-black text-gray-400 h-screen">
      <div className="z-40 sticky top-0">
        <CustomNavbar />
      </div>
      <div>
        <Routes>
          <Route path='/about' element={<AboutPage />} />
          <Route path='/cookieTest' element={<UserCookieTest />} />
          <Route path='/login' element={<UserLogin />} />
          <Route path='/signup' element={<UserSignup />} />
          <Route path='/generalTeamPage' element={<GeneralTeamPage />} />
          <Route path='/signupSuccess' element={<SignupSuccessPage />} />
          <Route path="/createTeamPage" element={<CreateTeamPage />} />
          <Route path='/' element={<HomePage />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
