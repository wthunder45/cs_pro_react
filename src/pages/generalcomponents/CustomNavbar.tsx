import React from 'react';
import { useLocation } from 'react-router-dom';
import Cookies from 'js-cookie';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';


function CustomNavbar() {

  const navigate = useNavigate();

  async function logout(e: React.SyntheticEvent) {
    e.preventDefault();
    try {
      const res = await axios.post('/user/userLogout');
      navigate('/');
      console.log(res);
    } catch (err) {
      console.error(err);
    }
  }

  const pathName = useLocation().pathname;

  return (
    <div className="CustomNavbar">
      <div className="flex mx-auto bg-black">
        <div className="flex w-full h-14 items-center text-center mx-2 space-x-3">
          <a href="/" className="subpixel-antialiased text-white text-xl font-bold">CS Notes</a>
          <div className="space-x-2">
            <a href="/" className="py-1 px-3 text-gray-500 rounded-sm">Home</a>
            <a href="/generalTeamPage" className="py-1 px-3 text-gray-500 rounded-sm">Teams</a>
            <a href="/about" className="py-1 px-3 text-gray-500 rounded-sm">About</a>
          </div>
          <div className="absolute right-0">
            <div className="inline-flex space-x-2 mx-2">
              <a href="/signup" className="bg-green-600 text-gray-200 w-20 rounded-sm py-2 px-3">Signup</a>
              <a href="/login" className="bg-blue-600 text-gray-200 w-20 rounded-sm py-2 px-3">Login</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CustomNavbar;
