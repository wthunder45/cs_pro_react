import React from 'react';
import A_Site_Inferno from '../../images/A_Site_Inferno.png';

function HomePage() {
  return (
    <div className="HomePage">
      <div className="z-0 ">
        <img src={A_Site_Inferno} className="absolute top-0 object-cover h-screen w-screen" />
      </div>
      <div className="relative text-lg z-30 bg-black ml-7 top-20 max-w-2xl rounded-md px-8 py-5">
        <h1>Welcome to CS Notes!</h1>
        <br />
        <p>
          This is a personal project of mine. By the time I've finished, I hope to have successfully
          deployed this application into production. The goal of CS Notes originally was to be a place
          where players can host their strategies to share with the community. Thus, these strats will
          be open to be critiqued and adopted by fellow players within the community. It would kind of
          mimic how open source software is forked, utilized, and modified collaboratively on Github/Gitlab.
        </p>
        <br />
        <p>
          I have pivoted and will be implementing features that will help teams recruit players and help
          players discover teams looking to recruit new players. The plan to implement a feature-rich
          editor for recording strategies is still going to be in the works, but due to the challenges of
          implementing it, it will be placed on the lower-priority list. This is truly just a project that
          I want to utilize to learn new technologies and deployment techniques.
        </p>
        <br />
        <p>
          For any business inquiries, please email me at: jnganguyen3@gmail.com.
        </p>
        <br />
        <p>
          Thanks for taking a look!
        </p>
        <p>- Jonathan Nguyen</p>
      </div>
    </div>
  )
}

export default HomePage;
