import React from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';

function CreateTeamPage() {
  return (
    <div className="CreateTeamPage Page">
      This is the create team page.
      <Container className="CreateTeamComponent">
        <Form>
          <Form.Group>
            <Form.Label>Team Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Team Name"
              size="lg"
              name="teamName"
              required
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Team Description</Form.Label>
          </Form.Group>
        </Form>
      </Container>
    </div>
  )
}

export default CreateTeamPage;