import React from 'react';
import { Card, Container, Row, Col, Button } from 'react-bootstrap';

function GeneralTeamPage() {
  return (
    <div className="mt-2 mx-4">
      <div className="container border-2 border-gray-200 max-w-sm rounded-sm">
        <h1>Team Name</h1>
        <a>Team Owner</a>
        <br />
        <a>ESEA League</a>
      </div>
    </div >
  )
}

export default GeneralTeamPage;