import React from 'react';

function SignupSuccessPage() {
  return (
    <div className="SignupSuccessPage Page">
      <h1>You successfully created an account!</h1>
      <p>
        Please proceed to login!
      </p>
    </div>
  )
}

export default SignupSuccessPage;