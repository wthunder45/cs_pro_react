import React, { useState, useEffect } from 'react';
import axios from 'axios';


// This component was made only to test if the jwt token was working
// It also confirmed that we can make api calls to express

function UserCookieTest () {
    const [data, setData] = useState('');

    useEffect( () => {
        async function fetchdata() {
            const result = await axios.get('/user/cookieTest');
            setData(result.data.message);
        }

        fetchdata()
    });

    return(
        <body>
            <div>this is the response from the server: {data}</div>
        </body>
    );
}

export default UserCookieTest;