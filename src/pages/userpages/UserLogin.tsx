import React, { useState } from 'react';
import { Form, Button, Container, Row } from 'react-bootstrap';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

function UserLogin() {

  const navigate = useNavigate();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    try {
      const response = await axios.post('/user/userLogin', {
        username: username,
        password: password
      });

      if (response.status === 200) navigate('/');
    } catch (err) {
      console.error(err);
    }
  }

  return (
    <div className="UserLogin">
      <Container className="AuthComponent">
        <h2>Login</h2>
        <Form onSubmit={handleSubmit}>
          <Row className="loginRow">
            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control
                onChange={e => setUsername(e.target.value)}
                size="lg"
                type="text"
                placeholder="enter username"
              />
            </Form.Group>
          </Row>
          <Row className="loginRow">
            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control
                onChange={e => setPassword(e.target.value)}
                type="password"
                size="lg"
                placeholder="enter password"
              />
            </Form.Group>
          </Row>
          <Row className="loginButton">
            <Button className="loginButton" variant="primary" type="submit">
              Submit
            </Button>
          </Row>
        </Form>
      </Container>

    </div>
  );
}

export default UserLogin;
