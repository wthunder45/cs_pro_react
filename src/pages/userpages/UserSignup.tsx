import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

function UserSignup() {
  let navigate = useNavigate();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passMatch, setPassMatch] = useState('');
  const [fname, setFName] = useState('');
  const [lname, setLName] = useState('');

  const handleSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    try {
      if (password != passMatch) { alert('The passwords do not match'); return; }

      const response = await axios.post('/user/createUser', {
        firstName: fname,
        lastName: lname,
        username: username,
        password: password
      });

      if (response.status === 200) navigate('/signupSuccess');
    } catch (err) {
      console.error(err);
    }
  }

  // We need to have inputs for username and password
  return (
    <div className="UserSignup">
      <form className="flex">
        <div>
          <label>Username</label>
          <input type="text" />
        </div>
        <div>
          <label>Password</label>
          <input type="password" />
        </div>
      </form>
    </div>
  );

}

export default UserSignup;
